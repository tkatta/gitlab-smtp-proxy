package main

import (
	"bytes"
	"html/template"
	"log"
	"os"

	"gopkg.in/gomail.v2"
)

type Smtp struct {
	server   string
	port     int
	user     string
	password string
}

type htmlTemplate struct {
	PipelineStatus string
	PipelineUrl    string
	Username       string
}

func (ht htmlTemplate) formHtmlTemplate(templatePath string) string {
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		log.Fatal("error parsing template:", err)
	}
	var body bytes.Buffer
	err = t.ExecuteTemplate(&body, "emailTemplate.html", ht)
	if err != nil {
		log.Fatal("error executing the template:", err)
	}
	return body.String()
}
func (s Smtp) sendGoEmail(emailBody string, subject string, attachment string, toList []string) {

	m := gomail.NewMessage()
	m.SetHeader("From", os.Getenv("GMAIL_USER"))
	m.SetHeader("To", toList...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", emailBody)
	m.Attach(attachment)

	d := gomail.NewDialer(s.server, s.port, s.user, s.password)
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}

	log.Println("Email has been sent Successfully..!")
}
func main() {
	log.Println("started gitlab email sender application")
	subject := "Test Golang Email Notification - 4"
	attach := "main.go"
	emailToList := []string{"issacnewton386@gmail.com", "sanskritinegi2401@gmail.com"}

	ht := htmlTemplate{
		PipelineStatus: "SUCCESS",
		PipelineUrl:    "https://gitlab.example.com/tkatta/pipelines/235",
		Username:       "Tinesh Katta",
	}

	smtp := Smtp{
		server:   "smtp.gmail.com",
		port:     587,
		user:     os.Getenv("GMAIL_USER"),
		password: os.Getenv("GMAIL_APP_PASS"),
	}

	emailBody := ht.formHtmlTemplate("emailTemplate.html")
	smtp.sendGoEmail(emailBody, subject, attach, emailToList)

}
